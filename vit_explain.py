import argparse
import sys
import torch
from PIL import Image
from torchvision import transforms
import numpy as np
import matplotlib.pyplot as plt
import cv2
import torchvision.models as models
import torch.nn as nn
from FOTCA import FOTCA_patch16_224_in21k as create_model
from vit_rollout import VITAttentionRollout
from vit_grad_rollout import VITAttentionGradRollout

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--use_cuda', action='store_true', default=True,
                        help='Use NVIDIA GPU acceleration')
    parser.add_argument('--image_path', type=str, default='./PlantVillage/Squash___Powdery_mildew/0ef07958-0cdf-4dad-a4de-b4d5a773f223___UMD_Powd.M 9978.JPG',
                        help='Input image path')# Corn_(maize)___Cercospora_leaf_spot Gray_leaf_spot/0e0ed08d-3021-49a7-9098-7c90afeb2fd5___RS_GLSp 4346.JPG
    # Cherry_(including_sour)___healthy/0b677e8e-e97d-4909-9dcd-a744fb6ac8a3___JR_HL 4260.JPG
    # Grape___Esca_(Black_Measles)/0ac1749b-6c4a-4a60-92d4-f4e957cfa4b4___FAM_B.Msls 0887.JPG
    # Pepper,_bell___Bacterial_spot/01dfb88b-cd5a-420c-b163-51f5fe07b74d___JR_B.Spot 9091.JPG
    # Squash___Powdery_mildew/0ef07958-0cdf-4dad-a4de-b4d5a773f223___UMD_Powd.M 9978.JPG
    parser.add_argument('--head_fusion', type=str, default='max',
                        help='How to fuse the attention heads for attention rollout. \
                        Can be mean/max/min')
    parser.add_argument('--discard_ratio', type=float, default=0.9,
                        help='How many of the lowest 14x14 attention paths should we discard')
    parser.add_argument('--category_index', type=int, default=None,
                        help='The category index for gradient rollout')
    args = parser.parse_args()
    args.use_cuda = args.use_cuda and torch.cuda.is_available()

    return args

def show_mask_on_image(img, mask):
    img = np.float32(img) / 255
    heatmap = cv2.applyColorMap(np.uint8(255 * mask), cv2.COLORMAP_JET)
    heatmap = np.float32(heatmap) / 255
    cam = heatmap + np.float32(img)
    cam = cam / np.max(cam)
    return np.uint8(255 * cam)

if __name__ == '__main__':
    args = get_args()
    model = create_model(num_classes=38, has_logits=False)
    pthfile = './vit_base_patch16_224_in21k.pth'
    # pthfile = './weights/改进的vit但是未数据增强.pth'
    weights_dict = torch.load(pthfile)
    print(model.load_state_dict(weights_dict, strict=False))
    model.eval()

    transform = transforms.Compose([
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.5, 0.5, 0.5], std=[0.5, 0.5, 0.5]),
    ])
    img = Image.open(args.image_path)
    img = img.resize((224, 224))
    input_tensor = transform(img).unsqueeze(0)
    # .cuda()

    if args.category_index is None:
        print("Doing Attention Rollout")
        attention_rollout = VITAttentionRollout(model, head_fusion=args.head_fusion,
            discard_ratio=args.discard_ratio)
        mask = attention_rollout(input_tensor)
        name = "attention_rollout_{:.3f}_{}.png".format(args.discard_ratio, args.head_fusion)
    else:
        print("Doing Gradient Attention Rollout")
        grad_rollout = VITAttentionGradRollout(model, discard_ratio=args.discard_ratio)
        mask = grad_rollout(input_tensor, args.category_index)
        name = "grad_rollout_{}_{:.3f}_{}.png".format(args.category_index,
            args.discard_ratio, args.head_fusion)

    print("1")
    np_img = np.array(img)[:, :, ::-1]
    mask = cv2.resize(mask, (np_img.shape[1], np_img.shape[0]))
    mask = show_mask_on_image(np_img, mask)
    cv2.imshow("Input Image", np_img)
    cv2.imshow(name, mask)
    cv2.imwrite("input.png", np_img)
    cv2.imwrite(name, mask)
    cv2.waitKey(-1)